## ¿Como ejecutar?

Simplemente hay que situarse en la carpeta del repositorio llamada "ud01a02" y ejecutar el siguiente comando:

```comand line
		
	java -jar .\target\ud01a02-1.0-SNAPSHOT.jar

```

Una vez ejecutado se podrán probar los tres ejercicios.