package ud01a02;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author Victor
 *
 */
public class App 
{
	// Ruta para almacenar los archivos de salida
	private static String pathOutput = "files/output.txt";
	private static String pathRandoms = "files/randoms.txt";
	private static String pathJarRandom = "java -jar jar/random10-1.0-SNAPSHOT.jar";
	private static String pathJarMinusculas = "java -jar jar/minusculas-1.0-SNAPSHOT.jar";
	private static String cmdExe = "cmd.exe";
	private static String c = "/c";
	
    public static void main( String[] args ) throws IOException
    {    	
    	// cmd.exe /c "ls -la"
    	//args[0] = "cmd.exe"; args[1] = "/c"; args[2] = "help";
    	
    	App app = new App();
    	Scanner scanner = new Scanner(System.in);
    	
    	System.out.println("¿Qué ejercicio deseas ejecutar? 1, 2 o 3...");
    	switch (scanner.nextInt()) {
		case 1:
			app.ejercicio1(args); 
			break;
		case 2:
			app.ejercicio2();
			break;
		case 3:
			app.ejercicio3();
			break;	
		default:
			System.err.println("Opción no válida");
			break;
		}  	
    }
    
    public void ejercicio1(String[] args) {
    	try {
    		// Variables para guardar la salida del comando en el fichero
    		FileWriter fileWriter = new FileWriter(App.pathOutput, true);
    		PrintWriter printWriter = new PrintWriter(fileWriter);
    		
    		Scanner scanner = new Scanner(System.in);
    		
    		System.out.println("¿Qué comando desea ejecutar?");
    		String command = scanner.nextLine();
    		
    		Process process = new ProcessBuilder(App.cmdExe, App.c, command).start();
    		// Tiempo de espera de 2 segundos 
    		process.waitFor(2, TimeUnit.SECONDS);

			/**
			 * En este apartado se ha de comprobar si el proceso se ha ejecutado
			 * correctamente. En tal caso, la salida estará en getInputStream 
			 * pero si no, la tendrás en getErrorStream().
			 * 
			 */

        	InputStream inputStream = process.getInputStream();
        	
        	InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        	BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        	
        	// Se guarda la salida del fichero en el archivo
        	printWriter.println("Ouput for the chil process " + Arrays.toString(args) + " : ");
        	
        	String linea;
        	while ((linea = bufferedReader.readLine()) != null) {
				/**
				 * Se ha de imprimir también por pantalla.
				 */
        		printWriter.println(linea);
    		}
        	printWriter.println("*** Fin de la ejecución");
        	
        	System.out.println("Se ha guardado la salida del fichero en " + App.pathOutput);
        	
        	// Se cierra el fichero

			/**
			 * Para cerrar el fichero, en Acceso a Datos os explicarán otra manera que también
			 * está muy bien para que no tengas que poner el fileWriter.close() porque debería
			 * de controlarse con un finally
			 */
        	fileWriter.close();
		} catch (IOException e) {
			System.out.println("The was an error while running " + Arrays.toString(args) + " --> " + e.getMessage());
		} catch (InterruptedException e) {
			System.out.println("The current process was interrupted --> " + e.getMessage());
		} catch (Exception e) {
            e.printStackTrace();
        }
	}
    
    public void ejercicio2() {    	
    	String readScanner = "start";
    	int count = 0;
    	
    	
    	/**
		 * Lo ideal para cerrar el Scanner que os lo comentará en acceso
		 * a datos es hacerlo de la siguiente manera: Ojo que se modifica
		 * el bloque try-catch
		 */
    	try (Scanner scanner = new Scanner(System.in);)  {

			/**
			* También podrías poner todas las variables siguientes,
			esto lo que garantiza es que siempre se llame al close de lo que 
			pongas
			*/

    		// Variables para guardar la salida del comando en el fichero
    		FileWriter fileWriter = new FileWriter(App.pathRandoms, true);
    		PrintWriter printWriter = new PrintWriter(fileWriter);
    		
	    	while (!readScanner.equals("stop")) {
	    		count++;
	    		
				/**
				 * No se ha de inicializar el proceso cada vez, se ha de inicializar antes y
				 * luego comunicarse con él.
				 * 
				 * Este código debería estar separado
				 */
	    		Process process = new ProcessBuilder(App.cmdExe, App.c, App.pathJarRandom).start();
	        	InputStream inputStream = process.getInputStream();     	
	        	InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	        	BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	        	String numRandom = bufferedReader.readLine();
	    		
				System.out.println("Escribe una palabra (stop para finalizar):");
				readScanner = scanner.nextLine();
	
	    		printWriter.println("Número " + count + ": " + numRandom); 
	    	}
	    	
	    	printWriter.println("*** Fin de la ejecución");	 
	    	
	    	System.out.println("Fin del programa.");
	    	
			/**
			 * Lo ideal es poner el fileWriter.close() en una
			 * sentencia finally dentro del try-catch
			 */
	    	// Se cierra el fichero
        	fileWriter.close();
		} catch (IOException e) {
			System.out.println("The was an error while running --> " + e.getMessage());
		} catch (Exception e) {
            e.printStackTrace();
        }
	}
    
    public void ejercicio3() {
    	
		/**
		 * Lo modifico como en el ejercicio anterior
		 */

		
		try (Scanner scanner = new Scanner(System.in);){
    		
			/**
			 * Falta el bucle
			 */

    		System.out.println("Escribe una frase para convertirla en minúscula:");
			String readScanner = scanner.nextLine();
			
			Process process = new ProcessBuilder(App.cmdExe, App.c, App.pathJarMinusculas + " \"" + readScanner + "\"").start();
        	InputStream inputStream = process.getInputStream();     	
        	InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        	BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        	System.out.println("Frase convertida: " + bufferedReader.readLine());        	
		} catch (IOException e) {
			System.out.println("The was an error while running --> " + e.getMessage());
		} catch (Exception e) {
            e.printStackTrace();
        }
	}
}
